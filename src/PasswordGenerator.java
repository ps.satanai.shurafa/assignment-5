import java.util.*;

public class PasswordGenerator {
    public StringBuilder generatePassword() {
        final String letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        final String special = "_$#%";
        final String numbers = "0123456789";
        final int passwordLength = 8;
        ArrayList<Character> password = new ArrayList<>(passwordLength);
        addRandomChars(special, 1, password);
        addRandomChars(numbers, 2, password);
        addRandomChars(letters, 1, password);
        addRandomChars(special, 1, password);
        addRandomChars(numbers, 2, password);
        addRandomChars(letters, 1, password);

        return getPassword(password);

    }

    private StringBuilder getPassword(ArrayList<Character> password) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Character character : password) {
            stringBuilder.append(character);
        }
        return stringBuilder;
    }

    private static void addRandomChars(String addition, int repeat, ArrayList<Character> password) {
        Random random = new Random();
        ArrayList<Character> temp = new ArrayList<>();
        HashSet<Character> set = new HashSet<>();
        boolean b;

        for (int i = 0; i < repeat; i++) {
            temp.add(addition.charAt(random.nextInt(addition.length() - 1)));
            b = set.addAll(temp);
            if (!b) repeat++;
        }
        password.addAll(set);

    }


    public static void main(String[] args) {
        PasswordGenerator passwordGenerator = new PasswordGenerator();
        System.out.println(passwordGenerator.generatePassword());
        System.out.println(passwordGenerator.generatePassword());
        System.out.println(passwordGenerator.generatePassword());
        System.out.println(passwordGenerator.generatePassword());
        System.out.println(passwordGenerator.generatePassword());
        System.out.println(passwordGenerator.generatePassword());

    }
}